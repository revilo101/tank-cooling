#! python2.7
import csv

# Weather object for external tank conditions
class Weather:

	def __init__(self):

		self.day_forcast = self.load_file('forecast_temps.csv')

		self.weighting_coefficients = self.load_file('weighting_coefficients.csv')

		month_historical = self.load_file('weather_historical.csv')[1:]
		self.ambient_temp = [float(x[0]) for x in month_historical]
		self.convection = [float(x[1]) for x in month_historical]
		self.solar_exposure = [float(x[2]) for x in month_historical]
		self.wind_speed = [float(x[3]) for x in month_historical]

		self.set_weighted_day_temps()

	# Load CSV files
	def load_file(self, file_name):
		with open(file_name, 'rb') as csvfile:
			reader = csv.reader(csvfile, delimiter=',')
			lst = []
			for row in reader:
				lst.append(row)
			return lst

	# Weighted month temperatures based on monthly ambient and weighting coeffs
	def set_weighted_day_temps(self):
		self.weighted_day_temps = []
		for i in range(12):
			self.weighted_day_temps.append(float(self.day_forcast[i][0]) + float(self.weighting_coefficients[i][0])*(float(self.day_forcast[i][1]) - float(self.day_forcast[i][0])))
		
	#Solar Radiation
	def get_solar_heat_input(self,month,tank_geometry):
		alpha = 0.27 #solar absorptivity of external coating

		Q_solar_3 = self.solar_exposure[month-1] * alpha * 0.5 * tank_geometry['A3'] #soalar heat input - outside tank wall [W]
		Q_solar_10 = self.solar_exposure[month-1] * alpha * tank_geometry['A10'] #solar heat input - tank top insulated [W]
		Q_solar_12 = self.solar_exposure[month-1] * alpha * tank_geometry['A12'] #soalar heat input - lid un-insulated [W]

		return Q_solar_3, Q_solar_10, Q_solar_12

	# Get ambient temperatures
	def get_ambient_temp(self,current_time,start_time):

		days_elapsed = current_time.day - start_time.day

		# If the days elapsed exceeds the amount of forcasted days, use monthly data
		if len(self.weighted_day_temps) <= days_elapsed - 1:
			ambient_temp = self.ambient_temp[current_time.month]
		
		# using forecast data
		else:
			ambient_temp = self.weighted_day_temps[days_elapsed]

		return ambient_temp
