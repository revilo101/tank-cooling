#! python2.7
import datetime
import csv
import math
import time

from weather import Weather
from tank import Tank

# # Get time to initially cool
# def initial_cooling_time(tank, weather):

# 	current_time = tank.time_start
# 	current_temp = tank.properties['Temperature_Initial']
# 	temperature_list = []
# 	ode_time = datetime.timedelta(seconds=0)

# 	# Cooling required
# 	if tank.properties['Temperature_Initial'] > tank.properties['Temperature_Setpoint_Upperbound']:

# 		tank.set_warming()
		
# 		while current_temp > tank.properties['Temperature_Setpoint_Upperbound']:

# 			ambient_temp = weather.weighted_day_temps[current_time.month]
# 			tank.constants['hc_out'] = weather.convection[current_time.month]

# 			Q_solar_3, Q_solar_10, Q_solar_12 = weather.get_solar_heat_input(current_time.month)

# 			current_temp = tank.get_wine_temp(ode_time.total_seconds(), current_temp, ambient_temp, Q_solar_3, Q_solar_10, Q_solar_12)
# 			temperature_list.append( [ current_time, current_temp ] )

# 			ode_time += datetime.timedelta(seconds=10)
# 			current_time += datetime.timedelta(seconds=10)


# 		print('Time to cool is ',ode_time.seconds//3600, (ode_time.seconds//60)%60)

# 	# Cooling not required
# 	else:
# 		print('No cooling required')

# Default cooling pattern
# def default_cooling(tank, weather):

# 	current_time = tank.time_start
# 	current_temp = tank.properties['Temperature_Initial']
# 	temperature_list = []
# 	cooling = False


# 	# Until the full time elapsed
# 	while current_time < tank.time_finish:

# 		ambient_temp = weather.get_ambient_temp(current_time, tank.time_start)
# 		tank.constants['hc_out'] = weather.convection[current_time.month]
# 		Q_solar_3, Q_solar_10, Q_solar_12 = weather.get_solar_heat_input(current_time.month)

# 		# Cooling if above temp upperbound
# 		if current_temp >= tank.properties['Temperature_Setpoint_Upperbound']:
# 			cooling = True

# 		if current_temp <= tank.properties['Temperature_Setpoint']:
# 			cooling = False


# 		tank.set_cooling() if cooling else tank.set_warming()

# 		current_temp = tank.get_wine_temp(datetime.timedelta(hours=1).total_seconds(), current_temp, ambient_temp, Q_solar_3, Q_solar_10, Q_solar_12)
# 		temperature_list.append( [cooling, round(current_temp,2) ] )

# 		# print([cooling, round(current_temp,2) ])
# 		current_time += datetime.timedelta(hours=1)


# General tank cooling function
# def tank_cool(tank):
	
# 	# Generate objects
# 	weather = Weather(tank)

# 	print(weather.weighted_day_temps)

# 	# initial_cooling_time(tank, weather)

# 	default_cooling(tank, weather)


weather = Weather()

temp_initial = 20.0 # deg
temp_setpoint = 5.0 # deg
temp_nightcool = 7.0 # deg

t_start = datetime.datetime(2017, 01, 01, 18, 30, 00)
t_finish = datetime.datetime(2017, 01, 04, 5, 40, 00)

tank = Tank(weather)

temps = tank.run_cooling_model(temp_initial,temp_setpoint,t_start,t_finish)
for temp in temps:
	print temp