#! python2.7
import math
import datetime
from weather import Weather

# Tank class for geometry, wine constants and general system properties
class Tank:

	def __init__(self,weather):


		self.weather = weather
		# self.time_start = time_start
		# self.time_finish = time_finish
		# self.run_time = time_finish - time_start

		# Geometry of Tank
		self.geometry = {
		'L': 5.7, #height of cylindrical section [m]
		't_door' : 0.002, #thickness of steel - door section [m]
		't_found' : 0.53, #thickness of foundation [m]
		't_ins' : 0.075, #thickenss of insulation [m]
		't_lid' : 0.002 , #thickness of lid [m]
		'A2' : 49.01, #Area of inside tank wall [m**2]
		'A3' : 52.7,  #Area of outside tank wall [m**2]
		'A4' : 1.08,  #Area of inside surface - manway [m**2]
		'A5' : 1.08,  #Area of outside surface - manway [m**2]
		'A6' : 6.4,   #Area of cooling jacket [m**2]
		'A7' : 4.176, #Area of tank bottom [m**2]
		'A9' : 1.244, #Area of wall math.exposed to internal air - inside [m**2]
		'A10' : 1.67, #Area of wall math.expose to internal aur - outside [m**2]
		'A11' : 0.4285,   #Area of lid - inside [m**2]
		'A12' : 0.4285,   #Area of lid - outside [m**2]
		'r2' : 1.155, #Radius of tank wall - inside [m**2]
		}

		self.geometry['r3'] = self.geometry['r2'] + self.geometry['t_ins'] #Radius of tank wall - outside [m**2]

		#System Properties
		self.properties = {
		'V_wine': 22230.0,     #Volume of wine [L]
		# 'Temperature_Initial':temp_initial,
		# 'Temperature_Setpoint':temp_setpoint, #Set Point Temperaure
		# 'Temperature_Nightcool':temp_nightcool,
		'Var_neg': 0.1,  #Negative Variation (lower set point)
		'Var_pos': 0.5,  #Positive Variation (upper set point)
		'Allowable_Variation':0.5,
		# 'step_cool': 30, #Time step for solver during cooling
		# 'step_warm': 600,  #Time step for solver during storage
		}

		# self.properties['Temperature_Nightcool_Uppperbound'] = self.properties['Temperature_Nightcool'] + 0.5
		# self.properties['Temperature_Setpoint_Upperbound'] = self.properties['Temperature_Setpoint'] + 0.5
		# self.properties['Temperature_Setpoint_Lowerbound'] = self.properties['Temperature_Setpoint'] - 0.5

		self.constants = {
		'hc_w_cool': 225.0,  #convection heat transfer coefficient during cooling - wine to ss [W/m**2K]
		'hc_w_warm': 150.0,  #convection heat transfer coefficient during warming - wine to ss [W/m**2K]
		'hc_air': 3.0, #convection heat transfer coefficent - air to ss (inside)
		'm_brine_on': 1.4505, #brine mass flow rate - during cooling [kg/s]
		'm_brine_off': 0,    #brine mass flow rate - during storage [kg/s]
		'w_in_on': 370.0,  #agitator work - during cooling (0.37KW = 370W)
		'w_in_off': 0,   #agitator work - during storage [W]
		'm_w': (self.properties['V_wine']/1000.0)*978.0, #mass - wine [kg]
		'alcohol_content': 11.0,
		'cp_brine': 3550.0, #specific heat brine [J/(kgk)]
		'k_ins': 0.041, #conduction heat transfer coefficient - insulation [W/mK]
		'k_steel': 16.20, #conduction heat transfer coefficient - stainless steel [W/mK]
		'k_found': 1.280, #conduction heat transfer coeffiencnt - foundation [W/mK]
		'T_soil': 14.1, #soil temperature
		'T_in': -9.24 #brine inlet temp
		}

		self.constants['cp_w'] = ((self.constants['alcohol_content']/100)*2460)+((1-(self.constants['alcohol_content']/100))*4184)   #specific heat - wine [J/(kgk)]


		self.electricity_cost_kWh = {
		'On Peak':0.080214 + 0.0307,
		'Off Peak':0.045204 + 0.0307
		}

	def get_wine_temp(self,t,y0,Ta):
		

		# y0 = self.properties['Temperature_Initial']

		for key,val in self.properties.items():
			exec(key + '=val')
		for key,val in self.geometry.items():
			exec(key + '=val')
		for key,val in self.constants.items():
			exec(key + '=val')

		#Substitution 1 
		a=hc_w*A2+(2*math.pi*k_ins*L)/(math.log(r3/r2))-((2*math.pi*k_ins*L/math.log(r3/r2))**2)/((2*math.pi*k_ins*L)/math.log(r3/r2)+hc_w*A3)
		b=((Ta*hc_w*A3+Q_solar_3)/((2*math.pi*k_ins*L)/math.log(r3/r2)+hc_w*A3))*((2*math.pi*k_ins*L)/math.log(r3/r2))
		c=(Ta*hc_out*k_steel/t_lid)/((k_steel/t_lid)+hc_out)
		d=(hc_w+(k_steel/t_lid)-((k_steel/t_lid)**2)/((k_steel/t_lid)+hc_out))
		f=2*m_brine*cp_brine*T_in
		e=(hc_w*A6+2*m_brine*cp_brine)
		h=T_soil*k_found/t_found
		g=(hc_w+k_found/t_found)
		i=((Ta*hc_out*A10+Q_solar_10)/((k_ins/t_ins)*A10+hc_out*A10))*(k_ins/t_ins)
		j=hc_air+(k_ins/t_ins)-((k_ins/t_ins)**2/((k_ins/t_ins)+hc_out)) 
		k=((Ta*hc_out*A12+Q_solar_12)/((k_steel/t_lid)*A12+hc_out*A12))*(k_steel/t_lid)
		l=hc_air+(k_steel/t_lid)-((k_steel/t_lid)**2/((k_steel/t_lid)+hc_out))
		
		#Substitution 2 
		m=hc_w*A2-((hc_w*A2)**2)/a
		n=b/a*hc_w*A2
		o=hc_w*A4-(hc_w**2*A4)/d
		p=c/d*hc_w*A4 
		q=hc_w*A6-(hc_w*A6)**2/e 
		r=f/e*hc_w*A6 
		s=hc_w*A7-(hc_w**2*A7)/g 
		t=h/g*hc_w*A7
		u=hc_air*A9-(hc_air**2*A9)/j 
		v=i/j*hc_air*A9 
		w=hc_air*A11-(hc_air**2*A11)/l 
		x=hc_air*A11*k/l
		
		AA=(m+o+q+s+u+w)/(-(m_w)*(cp_w))
		BB=(n+p+r+t+v+x+w_in)/(-(m_w)*(cp_w)) 

		C = (y0-BB/AA)#*math.exp(AA*ode_time) #correctly calculate the coefficient
		
		#for index in range(0,len(tspan)-1):
		T1 = BB/AA+C*math.exp(AA*t)
		return T1

	def set_warming(self):

		self.constants['m_brine'] = self.constants['m_brine_off']
		self.constants['hc_w'] = self.constants['hc_w_warm']
		self.constants['w_in'] = self.constants['w_in_off']

	def set_cooling(self):

		self.constants['m_brine'] = self.constants['m_brine_on']
		self.constants['hc_w'] = self.constants['hc_w_cool']
		self.constants['w_in'] = self.constants['w_in_on']

	def default_cooling(self,current_temp,setpoint_temp):

		# Cooling if above temp upperbound
		if current_temp >= setpoint_temp + self.properties['Allowable_Variation']:
			self.cooling = True

		if current_temp <= setpoint_temp:
			self.cooling = False

		self.set_cooling() if self.cooling else self.set_warming()

	def nightcool_cooling(self,current_temp,temp_setpoint):
		pass


	# Default cooling pattern
	def run_cooling_model(self,temp_initial,temp_setpoint,time_start,time_finish,increment = 60,nightcool = False):

		datetime.timedelta(minutes=increment).total_seconds()
		current_time = time_start
		current_temp = temp_initial
		temperature_list = []
		self.cooling = False


		# Until the full time elapsed
		while current_time < time_finish:

			ambient_temp = self.weather.get_ambient_temp(current_time, time_start)
			self.constants['hc_out'] = self.weather.convection[current_time.month]
			self.properties['Q_solar_3'], self.properties['Q_solar_10'], self.properties['Q_solar_12'] = self.weather.get_solar_heat_input(current_time.month,self.geometry)

			self.default_cooling(current_temp,temp_setpoint) if nightcool == False else self.nightcool_cooling(current_temp,temp_setpoint)

			current_temp = self.get_wine_temp(datetime.timedelta(minutes=increment).total_seconds(), current_temp, ambient_temp)

			temperature_list.append( [self.cooling, round(current_temp,2) ] )

			current_time += datetime.timedelta(minutes=increment)

		return temperature_list